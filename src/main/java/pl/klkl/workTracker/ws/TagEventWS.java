package pl.klkl.workTracker.ws;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.klkl.workTracker.domain.ITagEventRepository;
import pl.klkl.workTracker.domain.jaxb.Report;
import pl.klkl.workTracker.domain.jaxb.TagResult;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Component
@Path("event")
public class TagEventWS {
	
	@Autowired
    ITagEventRepository tagEventRepository;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Report getAllTags() {
		return tagEventRepository.getFullReport();
	}

    @GET
    @Path("monthly")
    @Produces(MediaType.APPLICATION_JSON)
    public Report balance(@QueryParam("year") Integer year, @QueryParam("month") Integer month){
        return tagEventRepository.getMonthlyReport(year, month);
    }
	
	@PUT
	@Path("tag")
    @Produces(MediaType.APPLICATION_JSON)
	public TagResult addTags(){
		return tagEventRepository.tag();
	}

}
