

package pl.klkl.workTracker.domain.jaxb;

import org.joda.time.DateTime;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Date: 16.02.2013
 * Time: 22:11
 */
@XmlRootElement(name = "tag-result")
public final class TagResult {
    private TagStatus tagStatus;
    private DateTime tagOn;
    private DateTime tagOff;

    public TagResult(TagStatus tagStatus, DateTime tagOn, DateTime tagOff) {
        this.tagStatus = tagStatus;
        this.tagOn = tagOn;
        this.tagOff = tagOff;
    }

    public TagResult() {
    }

    @XmlAttribute(name = "tag-status")
    public TagStatus getTagStatus() {
        return tagStatus;
    }

    @XmlAttribute(name = "tag-on-time")
    public DateTime getTagOn() {
        return tagOn;
    }

    @XmlAttribute(name = "tag-off-time")
    public DateTime getTagOff() {
        return tagOff;
    }

    @Override
    public String toString() {
        return "TagResult{" +
                "tagStatus=" + tagStatus +
                ", tagOn=" + tagOn +
                ", tagOff=" + tagOff +
                '}';
    }
}
