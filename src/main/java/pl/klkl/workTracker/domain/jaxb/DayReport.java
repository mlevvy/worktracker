package pl.klkl.workTracker.domain.jaxb;

import org.joda.time.DateTime;
import org.joda.time.Period;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Date: 16.02.2013
 * Time: 23:18
 */
@XmlRootElement(name = "day-report")
public class DayReport {
    private DateTime startTime;
    private DateTime endTime;
    private Period period;

    public DayReport() {
    }

    public DayReport(DateTime startTime, DateTime endTime) {
        this.startTime = startTime;
        this.endTime = endTime;
        period = new Period(startTime, endTime);
    }

    @XmlAttribute(name = "tag-on-time")
    public DateTime getStartTime() {
        return startTime;
    }

    @XmlAttribute(name = "tag-off-time")
    public DateTime getEndTime() {
        return endTime;
    }

    @XmlAttribute(name = "period")
    public Period getPeriod() {
        return period;
    }

    @Override
    public String toString() {
        return "DayReport{" +
                "startTime=" + startTime +
                ", endTime=" + endTime +
                ", period=" + period +
                '}';
    }
}
