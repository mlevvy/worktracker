package pl.klkl.workTracker.domain.jaxb.adapters;

import org.joda.time.DateTime;

import javax.xml.bind.annotation.adapters.XmlAdapter;

/**
 * Date: 16.02.2013
 * Time: 23:48
 */
public class DateTimeAdapter extends XmlAdapter<String, DateTime> {

    public DateTime unmarshal(String v) throws Exception {
        return new DateTime(v);
    }

    public String marshal(DateTime v) throws Exception {
        if(v == null){
            return null;
        }
        return v.toString();
    }

}
