package pl.klkl.workTracker.domain.jaxb;

/**
 * Date: 16.02.2013
 * Time: 22:33
 */
public enum TagStatus {
    TAG_ON, TAG_OFF
}
