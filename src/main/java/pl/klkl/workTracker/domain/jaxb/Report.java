package pl.klkl.workTracker.domain.jaxb;

import org.joda.time.Period;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * Date: 16.02.2013
 * Time: 23:18
 */
@XmlRootElement(name = "monthly-report")
public final class Report {
    private List<DayReport> daysReports;
    private Period totalPeriod;

    public Report() {
    }

    public Report(List<DayReport> daysReports, Period totalPeriod) {
        this.daysReports = daysReports;
        this.totalPeriod = totalPeriod;
    }

    @XmlElement(name = "day-reports")
    public List<DayReport> getDaysReports() {
        return daysReports;
    }

    @XmlAttribute(name = "total-work-time")
    public Period getTotalPeriod() {
        return totalPeriod;
    }

    @Override
    public String toString() {
        return "Report{" +
                "daysReports=" + daysReports +
                ", totalPeriod=" + totalPeriod +
                '}';
    }
}
