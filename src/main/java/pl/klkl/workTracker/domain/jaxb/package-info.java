@XmlJavaTypeAdapters({@XmlJavaTypeAdapter(type=DateTime.class, value=DateTimeAdapter.class),@XmlJavaTypeAdapter(type=Period.class, value= PeriodAdapter.class)})
package pl.klkl.workTracker.domain.jaxb;

import org.joda.time.DateTime;
import org.joda.time.Period;
import pl.klkl.workTracker.domain.jaxb.adapters.DateTimeAdapter;
import pl.klkl.workTracker.domain.jaxb.adapters.PeriodAdapter;

import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapters;



