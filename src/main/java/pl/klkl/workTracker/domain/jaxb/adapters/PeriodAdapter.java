package pl.klkl.workTracker.domain.jaxb.adapters;

import org.joda.time.Period;

import javax.xml.bind.annotation.adapters.XmlAdapter;

/**
 * Date: 16.02.2013
 * Time: 23:59
 */
public class PeriodAdapter extends XmlAdapter<String, Period> {

    @Override
    public Period unmarshal(String p) throws Exception {
        return new Period(p);
    }

    @Override
    public String marshal(Period p) throws Exception {
        return p.toString();
    }
}