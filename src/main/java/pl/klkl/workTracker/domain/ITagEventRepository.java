package pl.klkl.workTracker.domain;

import pl.klkl.workTracker.domain.jaxb.Report;
import pl.klkl.workTracker.domain.jaxb.TagResult;

public interface ITagEventRepository {

	public TagResult tag();

	public Report getFullReport();

    public Report getMonthlyReport(Integer year, Integer month);

}
