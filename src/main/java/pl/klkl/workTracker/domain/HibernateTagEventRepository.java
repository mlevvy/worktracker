package pl.klkl.workTracker.domain;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.joda.time.DateTime;
import org.joda.time.Period;
import org.springframework.transaction.annotation.Transactional;
import pl.klkl.workTracker.domain.dto.TagEvent;
import pl.klkl.workTracker.domain.jaxb.DayReport;
import pl.klkl.workTracker.domain.jaxb.Report;
import pl.klkl.workTracker.domain.jaxb.TagResult;
import pl.klkl.workTracker.domain.jaxb.TagStatus;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class HibernateTagEventRepository implements ITagEventRepository {

	private final SessionFactory sessionFactory;

	public HibernateTagEventRepository(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	@Transactional
	public TagResult tag() {
        Query query = session().createQuery("from TagEvent order by id desc");
        TagEvent lastEvent = (TagEvent) query.setMaxResults(1).uniqueResult();

        if(lastEvent==null || (lastEvent.getStartTime() != null && lastEvent.getEndTime() !=null) ){
            //TAG ON
            TagEvent tagEvent = new TagEvent(new DateTime());
            session().save(tagEvent);
            return new TagResult(TagStatus.TAG_ON, tagEvent.getStartTime(),null);
        }
        else {
            //TAG OFF
            lastEvent.setEndTime(new DateTime());
            return new TagResult(TagStatus.TAG_OFF, lastEvent.getStartTime(), lastEvent.getEndTime());
        }
    }

	@Override
	@Transactional(readOnly = true)
	@SuppressWarnings("unchecked")
	public Report getFullReport() {
        return getReport(session().createQuery("from TagEvent").list());
	}

    @Override
    @Transactional(readOnly = true)
    public Report getMonthlyReport(Integer year, Integer month) {
        DateTime firstDay = new DateTime(year,month,1,0,0);
        DateTime lastDay = firstDay.dayOfMonth().withMaximumValue().hourOfDay().withMaximumValue().minuteOfDay().withMaximumValue();

        Query query = session().createQuery("from TagEvent where startTime >= :firstDay and endTime <= :lastDay");
        query.setParameter("firstDay",firstDay);
        query.setParameter("lastDay",lastDay);

        return getReport((List<TagEvent>) query.list());
    }

    private Session session() {
        return sessionFactory.getCurrentSession();
    }

    private Report getReport(List<TagEvent> tagEvents){
        List<DayReport> dayReports = new ArrayList<>();
        for(TagEvent tagEvent : tagEvents){
            DayReport dayReport = new DayReport(tagEvent.getStartTime(), tagEvent.getEndTime());
            dayReports.add(dayReport);
        }
        return new Report(Collections.unmodifiableList(dayReports), getPeriod(dayReports));
    }

    private Period getPeriod(List<DayReport> dayReports){
        Period totalPeriod = new Period();
        for(DayReport dayReport : dayReports){
            totalPeriod = totalPeriod.plus(dayReport.getPeriod());
        }
        return totalPeriod;
    }
}